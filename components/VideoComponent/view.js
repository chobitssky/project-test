import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import { Grid, Typography, Box } from "@mui/material";
import ListCard from "./ListCard";
import VideoService from "../../services/video.service";
import ViewCard from "./ViewCard";
import * as _ from "lodash";

const VideoViewComponent = () => {
  const router = useRouter();
  const { id } = router.query;

  const [videos, setVideos] = useState(null);

  useEffect(() => {
    const loadVideo = async () => {
      const response = await VideoService.getVideoById(id);
      if (response.status === 200) {
        if (!_.isEmpty(response.data)) {
          setVideos(response.data);
        }
      }
    };

    loadVideo();
  }, [id]);

  return (
    <>
      {videos ? (
        <ViewCard item={videos} />
      ) : (
        <Box display="flex" my={4}>
          <Typography flex={1} variant="span" textAlign="center">
            Video not found
          </Typography>
        </Box>
      )}
    </>
  );
};
export default VideoViewComponent;
