import * as React from "react";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import Typography from "@mui/material/Typography";
import { CardActionArea } from "@mui/material";

export default function ListCard({ item }) {
  return (
    <CardActionArea href={`/video/${item?.video_tyid ?? null}`}>
      <Card sx={{ maxWidth: "100%", height: "100%" }}>
        <CardMedia
          component="img"
          height="150"
          image={`https://img.youtube.com/vi/${item?.video_tyid}/hqdefault.jpg`}
          alt="green iguana"
        />
        <CardContent sx={{ py: 1.5 }}>
          <Typography gutterBottom variant="h5" fontSize={18} component="div">
            {item?.video_name ?? ""}
          </Typography>
          <Typography
            variant="body2"
            color="text.secondary"
            sx={{
              overflow: "hidden",
              textOverflow: "ellipsis",
              display: "-webkit-box",
              WebkitLineClamp: "2",
              WebkitBoxOrient: "vertical",
            }}
          >
            {item?.video_description ?? ""}
          </Typography>
        </CardContent>
      </Card>
    </CardActionArea>
  );
}
