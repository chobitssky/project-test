import * as React from "react";
import { useParams } from "next/navigation";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import Typography from "@mui/material/Typography";
import { Box, Button, CardActionArea, Divider } from "@mui/material";
import YoutubeEmbed from "../YoutubeEmbled";
import { useRouter } from "next/router";

export default function ViewCard({ item }) {
  const router = useRouter();
  return (
    <Box>
      <Box>
        <YoutubeEmbed embedId={item?.video_tyid} />
      </Box>
      <Box display="flex" flexDirection="column" spacing={4} my={2}>
        <Typography component="div" variant="h4" fontWeight={600} fontSize={42}>
          {item?.video_name}
        </Typography>
        <Divider sx={{ my: 3 }} />
        <Typography component="div" variant="span">
          {item?.video_description}
        </Typography>
      </Box>
      <Box my={4}>
        <Button variant="outlined" onClick={(e) => router.push("/")}>
          Back to Main Menu
        </Button>
      </Box>
    </Box>
  );
}
