import { useEffect, useState } from "react";
import { Grid, Typography, Box } from "@mui/material";
import ListCard from "./ListCard";
import VideoService from "../../services/video.service";

const VideoComponent = () => {
  const [videos, setVideos] = useState([]);

  useEffect(() => {
    const loadVideo = async () => {
      const response = await VideoService.getVideo();
      if (response.status === 200) {
        setVideos(response.data);
      } else {
        setVideos([]);
      }
    };

    loadVideo();
  }, []);

  return (
    <>
      {videos ? (
        <Grid alignItems="stretch" container columnSpacing={2} rowSpacing={4}>
          {videos?.map((item, index) => (
            <Grid display="flex" key={index} item md={3} sm={6} xs={12}>
              <ListCard item={item} />
            </Grid>
          ))}
        </Grid>
      ) : (
        <Box display="flex" my={4}>
          <Typography flex={1} variant="span" textAlign="center">
            Loading...
          </Typography>
        </Box>
      )}
    </>
  );
};
export default VideoComponent;
