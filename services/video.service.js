import axios from "axios";

class VideoService {
  getVideo = async () => {
    return await axios.post("/api/videos");
  };
  getVideoById = async (id) => {
    return await axios.post(`/api/videos/${id}`);
  };
}

export default new VideoService();
