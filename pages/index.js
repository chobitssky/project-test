import Head from "next/head";
// MUI
import { Box, Container } from "@mui/material";
// Component
import Header from "../components/HeaderComponent";
import VideoComponent from "../components/VideoComponent";

export default function Home() {
  return (
    <Box>
      <Head>
        <title>SITTHA TEST</title>
        <meta name="description" content="Next apps" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Header />
      <Container maxWidth="lg" sx={{ my: 4 }}>
        <VideoComponent />
      </Container>
    </Box>
  );
}
