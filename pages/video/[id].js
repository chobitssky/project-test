import Head from "next/head";
import { useParams } from "next/navigation";

import { Box, Container } from "@mui/material";

import Header from "../../components/HeaderComponent";
import VideoViewComponent from "../../components/VideoComponent/view";

export default function View() {
  return (
    <Box>
      <Head>
        <title>SITTHA TEST</title>
        <meta name="description" content="Next apps" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Header />
      <Container maxWidth="lg" sx={{ my: 4 }}>
        <VideoViewComponent />
      </Container>
    </Box>
  );
}
