// Next.js API route support: https://nextjs.org/docs/api-routes/introduction

import videos from "../../../mockup/video.json";
export default function handler(req, res) {
  res.status(200).json(videos);
}
