import videos from "../../../mockup/video.json";

export default function handler(req, res) {
  const query = req.query;
  const { id } = query;

  const item = videos.find((item) => item.video_tyid === id);

  res.status(200).json(item ?? {});
}
